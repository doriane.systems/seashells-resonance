#!/usr/bin/python

import os, shutil
import bin.configs as configs

def delete_folder_content(path):
    for filename in os.listdir(path):
        file_path = os.path.join(path, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

if __name__ == '__main__':
    # by default delete all historic, cache and images (reset)
    delete_folder_content(configs.CACHE)
    delete_folder_content(configs.COVERS_PATH)
