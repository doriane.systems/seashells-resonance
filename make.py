#!/usr/bin/python

import sys
import os
from jinja2 import Template
import musicbrainzngs
import json
from markdown import markdown

import bin.configs as configs
from bin.parse import yaml_parsing, md_parsing
from bin.cache import (
    init_cache,
    add_to_historic,
    get_from_historic,
    add_to_cache,
    get_from_cache
)
from bin.request import search_music
from bin.formate import make_query, result_to_formated, input_to_formated


#   ===========================================================================

def get_from_cached_results(query, historic, m_input):
    id = get_from_historic(query, historic)
    if "not_found" not in id:
        cached_result = get_from_cache(id)
        m_formated = result_to_formated(cached_result, m_input)
        print("back from cache:")
        print("  " + m_formated['name'] + " - " + ", ".join(m_formated['artist']))
    else:
        m_formated = input_to_formated(m_input)
        print("using custom input:")
        print("  " + m_formated['name'] + " - " + ", ".join(m_formated['artist']))
    return m_formated

def get_from_API(query, m_input, historic):

    # --- search for release
    print("searching:")
    if 'single_track' in m_input:
        m_result = search_music(query, m_input['single_track'])
    else:
        m_result = search_music(query)

    if m_result:

        # NOTE: we cache the request result and not the formated version,
        # so if the user change custom tags, they are going to change
        # in the render even though we have an entry in the cache
        add_to_cache(m_result)
        add_to_historic(query, m_result['id'], historic)

        m_formated = result_to_formated(m_result, m_input)
        print("cached:")
        print("  " + m_formated['name'] + " - " + ", ".join(m_formated['artist']))

    else:
        m_formated = input_to_formated(m_input)
        print("using custom input:")
        print("  " + m_formated['name'] + " - " + ", ".join(m_formated['artist']))
        add_to_historic(query, m_formated['id'], historic)

    return m_formated

def process_music_input(m_input, historic):

    query = ''
    if 'id' in m_input:
        query = "direct_id:" + m_input['id']
    elif 'custom' in m_input:
        query = "[custom]"
    else:
        query = make_query(m_input)
        if 'single_track' in m_input:
            query = query + " AND single_track:true"

    print("Query:")
    print("\"" + query + "\"")

    if '[custom]' in query:
        m_formated = input_to_formated(m_input)
        print("using custom input:")
        print("  " + m_formated['name'] + " - " + ", ".join(m_formated['artist']))
    elif query in historic:
        m_formated = get_from_cached_results(query, historic, m_input)
    else:
        m_formated = get_from_API(query, m_input, historic)

    print("*")
    return m_formated


#   MAIN
#   ===========================================================================

def main(argv):

    possible_extensions = [".yaml", "yml"]

    # --- parsing argv
    if not argv:
        print("Precise the yaml you want to use as input:")
        for root, dirs, files in os.walk(configs.LISTS):
            for name in files:
                (basename, ext) = os.path.splitext(name)
                if ext in possible_extensions:
                    print(" * " + os.path.join(root,name))
            sys.exit(1)
    
    if not os.path.isfile(argv[0]):
        sys.exit("Error: {f} is not a file".format(f=argv[0]))

    elif os.path.splitext(argv[0])[1] not in possible_extensions:
        sys.exit("Error: {f} is not a yaml file".format(f=argv[0]))

    # --- initialisation (parsing)
    input_lists = yaml_parsing(argv[0])
    if 'title' in input_lists:
        title = input_lists['title']
    else:
        title = ""
    if 'intro' in input_lists:
        intro = markdown(input_lists['intro'])
    else:
        intro = ""


    print("MAKING MUSIC LISTS:")
    print(title)
    print("------------------------")


    # --- cache initialisation
    historic = init_cache()

    # --- musicbrainz auth
    # Tell musicbrainz what your app is, and how to contact you
    # (this step is required, as per the webservice access rules
    # at http://wiki.musicbrainz.org/XML_Web_Service/Rate_Limiting )
    project_name = "seashell-resonance"
    project_version = "0.1"
    project_url = "https://gitlab.com/doriane.systems/seashells-resonance"
    musicbrainzngs.set_useragent(project_name, project_version, project_url)

    # --- processing the input music lists
    for list in input_lists['lists'].values():
        if 'description' in list:
            list['description'] = markdown(list['description'])
        list['music'][:] = [ process_music_input(music, historic) for music in list['music'] ]

    # --- rendering the template
    with open(configs.TEMPLATE_PATH, 'r') as file:
        template = Template(file.read())

    html = template.render(title = title, intro = intro, music_lists = input_lists['lists'], display = configs.display, project_url=project_url)
    basename = os.path.splitext(os.path.basename(argv[0]))[0]
    html_path = os.path.join(configs.RENDER, basename + ".html") 
    with open(html_path, 'w') as file:
        file.write(html)

    print('-> ' + html_path + ' was generated')


if __name__ == '__main__':
    main(sys.argv[1:])