#!/usr/bin/python

import bin.configs as configs
from bin.parse import date_parsing, make_lucene
import json

#   FORMATING
#   ===========================================================================

def make_query(music):
    # the query must be formulated in lucene syntax
    params = ['name', 'artist']
    opt_params = ['date', 'type']

    data = {}
    for p in params:
        if p in music:
            data[p] = music[p]
        else:
            print("WARNING: missing " + p + " parameter")
    for op in opt_params:
        if op in music:
            data[op] = music[op]
            
    query = make_lucene(data)
    return query

def input_to_formated(m_input):

    m_formated = m_input

    m_formated['id'] = "not_found"
    m_formated['artist'] = [m_input['artist']]

    if 'single_track' not in m_formated:
        m_formated['single_track'] = False

    # release specific formating
    if not m_formated['single_track']:
        if 'type' in m_input:
            m_formated['type'] = m_input['type']
        if 'labels' in m_input:
            m_formated['labels'] = m_input['labels'].split(', ')

        # --- covers
        m_formated['cover'] = {'type': 'default'} if not 'cover' in m_input else {
            'type': 'image',
            'default_format': m_input['cover']
        }

    # single track specific formating
    else:
        if 'releases' in m_input:
            m_formated['releases'] = [ {'title': release['title'], 'date': date_parsing(release['date'])} for release in m_input['releases']]

    if 'date' in m_input:
        m_formated['date'] = date_parsing(m_input['date'])
        
    if 'tags' in m_input:
        m_formated['tags'] = m_input['tags'].split(', ')

    return m_formated

def result_to_formated(m_result, m_input):

    m_formated = m_input

    if 'single_track' not in m_formated:
        m_formated['single_track'] = False

    # those are always the musicbrainz result
    m_formated['id'] = m_result['id']
    m_formated['name'] = m_result['title']
    m_formated['artist'] = [artist['artist']['name'] for artist in m_result['artist-credit'] if type(artist) is dict]

    # release specific formating
    if not m_formated['single_track']:

        # release type (album, ep, single, compilation)
        m_formated['type'] = m_result['type']

        # the first link with bandcamp in target
        if not 'link' in m_input:
            if 'url-relation-list' in m_result:
                valid_links = [link['target'] for link in m_result['url-relation-list'] if 'bandcamp' in link['target']]
                if valid_links:
                    m_formated['link'] = valid_links[0]

        # a list of label on which it was released
        if 'label-info-list' in m_result:
            m_formated['labels'] = [ label['label']['name'] for label in m_result['label-info-list'] if label['label']['name'] != "[no label]"]

        # the date of release
        m_formated['date'] = date_parsing(m_result['date'])

        # cover artwork
        # covert art archive gives us:
        # 1200w (not always), 500w, 250w, large(=500), small(=250)
        m_formated['cover'] = {}
        if m_result['cover']:
            m_formated['cover']['type'] = 'image'
            formats = m_result['cover']['thumbnails']
            if configs.local_covers:
                formats = {
                    '500': configs.COVERS_PATH.split("/")[1] + "/" + m_result['id'] + '.jpg'
                }
            m_formated['cover']['formats'] = [ url + " " + size + "w" for size, url in formats.items() ]
            m_formated['cover']['default_format'] = formats['500']
        else:
            m_formated['cover']['type'] = 'default'

    # single track specific formating
    else:
        # this is a list of releases not release-groups
        # by putting it in a dic with title has hash, it'll remove every result we have in two
        # without having to do a request group for every entry
        m_formated['releases'] = {release['title']: {'title': release['title'], 'date': date_parsing(release['date'])}
                                    for release in m_result['release-list']}
        m_formated['releases'] = [ release for hash, release in m_formated['releases'].items()]

    # tags are always taken from m_input
    if 'tags' in m_input:
        m_formated['tags'] = m_input['tags'].split(', ')

    return m_formated
