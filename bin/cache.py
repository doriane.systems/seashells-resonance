#!/usr/bin/python

import os
import json

import bin.configs as configs
from bin.parse import (
    json_parsing,
    json_writing
)


#   CACHE
#   ===========================================================================

def init_cache():
    # create historic file if doesn't exist, otherwise open it
    if not os.path.exists(configs.HISTORIC_PATH):
        historic = {}
        json_writing(configs.HISTORIC_PATH, historic)
    else:
        historic = json_parsing(configs.HISTORIC_PATH)

    # create cache directory if not exist
    if not os.path.exists(configs.CACHE_PATH):
        os.makedirs(configs.CACHE_PATH)

    return historic

def add_to_historic(query, id, historic):
    historic[query] = id
    json_writing(configs.HISTORIC_PATH, historic)

def get_from_historic(query, historic):
    return historic[query]

def add_to_cache(m_result):
    relative_path = configs.CACHE_PATH + m_result['id'] + '.json'
    json_writing(relative_path, m_result)

def get_from_cache(id):
    relative_path = configs.CACHE_PATH + id + '.json'
    return json_parsing(relative_path)
