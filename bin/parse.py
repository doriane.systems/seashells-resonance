#!/usr/bin/python

import yaml
import json
from markdown import markdown
import datetime


#   PARSING
#   ===========================================================================

def yaml_parsing(NAME_PATH):
    yml = ""
    with open(NAME_PATH, 'r') as file:
        yml = yaml.safe_load(file)
    return yml

def json_parsing(NAME_PATH):
    jsn = ""
    with open(NAME_PATH, 'r') as file:
        jsn = json.load(file)
    return jsn

def json_writing(NAME_PATH, data):
    with open(NAME_PATH, 'w') as file:
        json.dump(data, file, indent=4, default=str)

def md_parsing(NAME_PATH):
    md = ""
    with open(NAME_PATH, 'r') as file:
        md = markdown(file.read())
    return md

def make_lucene(data):
    lucene = ' AND '.join([ ':'.join([field, str(data)]) for field, data in data.items()])
    return lucene

def deconstruct_lucene(lucene):
    data = { field.split(":")[0]:field.split(":")[1] for field in lucene.split(" AND ")}
    return data

def date_parsing(datestr):
    for fmt in ('%Y-%m-%d', '%Y-%m', '%Y'):
        try:
            return datetime.datetime.strptime(datestr, fmt)
        except ValueError:
            pass

def date_writing(date):
    return datetime.datetime.strftime(date, '%Y-%m-%d')
