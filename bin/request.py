#!/usr/bin/python

from cmath import log
from typing import final
import musicbrainzngs
import requests
from bin.parse import deconstruct_lucene
import bin.configs as configs
import json
import os


#   API CALLS
#   ===========================================================================

def search_release(query):
    results = []
    result = {}

    print("  search release")
    try:
        results = musicbrainzngs.search_releases(query=query, strict=True)['release-list']
    except musicbrainzngs.WebServiceError as e:
        print("    {e}".format(e=e))
    if results:
        # result are ordered by relevance
        result = results[0]
        print("    found: " + result['id'])
    else:
        print("   not found, but you can add the release yourself to the musicbrainz database!")
        print("   https://musicbrainz.org/doc/How_to_Add_a_Release")
    return result

def search_recording(query):
    results = []
    result = {}

    print("  search recording")
    try:
        results = musicbrainzngs.search_recordings(query=query, strict=True)['recording-list']
    except musicbrainzngs.WebServiceError as e:
        print("    {e}".format(e=e))
    if results:
        # result are ordered by relevance
        result = results[0]
        print("    found: " + result['id'])
    else:
        print("   not found, but you can add the recording yourself to the musicbrainz database!")
        print("   https://musicbrainz.org/doc/How_to_Add_a_Release")
    return result

def request_group(id):
    group = {}
    includes = ['releases', 'media']

    print("  request group: " + id)
    try:
        group = musicbrainzngs.get_release_group_by_id(id, includes= includes)['release-group']
    except musicbrainzngs.WebServiceError as e:
        print("    {e}".format(e=e))

    return group

def request_release(id):
    release = {}
    includes = ['artist-credits', 'release-groups', 'url-rels', 'labels']

    print("  request release: " + id)
    try:
        release = musicbrainzngs.get_release_by_id(id, includes=includes)['release']
    except musicbrainzngs.WebServiceError as e:
        print("    {e}".format(e=e))

    return release

def request_recording(id):
    recording = {}
    includes = ['artist-credits', 'releases']

    print("  request recording: " + id)
    try:
        recording = musicbrainzngs.get_recording_by_id(id, includes=includes)['recording']
    except musicbrainzngs.WebServiceError as e:
        print("    {e}".format(e=e))

    return recording

def request_cover(id):
    cover = {}

    data = ""
    print("  request cover")
    try:
        data = musicbrainzngs.get_image_list(id)
    except musicbrainzngs.WebServiceError as e:
        print("    {e}".format(e=e))

    if data:
        for image in data['images']:
            if 'Front' in image['types'] and image['approved']:
                cover = image

                # homogenise cover format, only keeping pixels keys
                # large == 500, small == 250
                formats = cover['thumbnails']
                if 'large' in formats:
                    formats['500'] = formats['large']
                    del formats['large']
                if 'small' in formats:
                    formats['250'] = formats['small']
                    del formats['small']
                break

    return cover


def download_cover(music_result):

    local_url = configs.COVERS_PATH + music_result['id'] + '.jpg'
                
    # if local_covers and already there, do nothing
    if os.path.exists(local_url):
        print("  cover already in local folder")

    # else download
    else:
        print("  saving cover in local folder")
        local_url = configs.COVERS_PATH + music_result['id'] + '.jpg'
        # TODO: only downloading the 500px one for now
        image_url = music_result['cover']['thumbnails']['500']
        img_data = requests.get(image_url).content
        with open(local_url, 'wb') as handler:
            handler.write(img_data)


#   MAIN SEARCH PROCESS
#   ===========================================================================

def search_music(query, single_track=False):
    final_result = {}

    if 'direct_id:' in query:

        print("  direct request with id")
        direct_id = deconstruct_lucene(query)['direct_id']

        if not single_track:
            final_result = request_release(direct_id)
            if final_result:
                group = request_group(final_result['release-group']['id'])
                final_result['type'] = group['type']
                final_result['cover'] = request_cover(final_result['id'])
                if configs.local_covers and final_result['cover']:
                    download_cover(final_result)

        else:
            final_result = request_recording(direct_id)
            print(json.dumps(final_result, indent=4))

    else:

        if not single_track:

            # search for release and then go to its group
            # as release-group search does not allow to input date and type
            start_result = search_release(query)

            if start_result:

                group = request_group(start_result['release-group']['id'])
                group_results = group['release-list']
                print("    found " + str(len(group_results)) + " associated releases")

                # a group is always of the same release type
                # so the final result is going to be of the same release type than the searched one
                # but it can contains different date entry, we have to filter only the one of the precised date
                # we know there is at least one because we got the group from a release of this searched date
                if 'date' in query:
                    possible_dates = [result['date'] for result in group_results if 'date' in result]
                    print("    possible dates: " + ", ".join(possible_dates))
                    # filtering for corresponding date
                    date = deconstruct_lucene(query)['date']
                    group_results = [ result for result in group_results
                                    if 'date' in result and date in result['date'] ]

                    selected_dates = [result['date'] for result in group_results]
                    print("    selected dates: " + ", ".join(selected_dates))

                # filtering for digital releases
                digital_results = [ result for result in group_results
                                    if result['medium-list'][0]['format'] == 'Digital Media' ]

                if digital_results:
                    final_result = request_release(digital_results[0]['id'])
                else:
                    print("    no digital release in group")
                    final_result = request_release(group_results[0]['id'])

                # adding type the dict
                # Note: type is a release-group property, and not a release one
                final_result['type'] = group['type']

                # request cover and add it to the dict
                final_result['cover'] = request_cover(final_result['id'])
                if configs.local_covers and final_result['cover']:
                    download_cover(final_result)
        
        else:
            true_query = ' AND '.join(query.split(" AND ")[:-1])
            final_result = search_recording(true_query)
            # if final_result:
            #     print(json.dumps(start_result, indent=4))


    return final_result
