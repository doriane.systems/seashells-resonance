#!/usr/bin/python

# options
display = 'both'    # can be: 'both', 'list' or 'grid'
local_covers = True;

# materials paths
MATERIALS = 'materials/'
TEMPLATE_PATH = MATERIALS+'template.html'

# music lists path
LISTS = 'music_lists/'

# cache paths
CACHE = 'cache/'
HISTORIC_PATH = CACHE+'historic.json'
CACHE_PATH = CACHE+'cached_results/'

# render paths
RENDER = 'www/'
COVERS_PATH = RENDER+'covers/'
